# 1. Contexte du document : Segmentation d'image multi-class pour véhicule autonome

Dans le paysage technologique actuel, les véhicules autonomes représentent une avancée majeure vers des systèmes de transport plus sûrs, efficaces et durables. Au cœur de cette révolution, la segmentation d'images multi-class joue un rôle crucial, permettant aux véhicules de comprendre et de naviguer dans leur environnement de manière autonome.

Future Vision Transport, une entreprise pionnière dans la conception de systèmes embarqués de vision par ordinateur, se trouve à l'avant-garde de cette innovation.

Au sein de l'équipe R&D de Future Vision Transport, l'objectif de notre projet est de développer un modèle performant de segmentation d'images qui peut identifier avec précision et fiabilité divers éléments urbains capturés par les caméras embarquées dans les véhicules.
Ces éléments incluent, mais ne se limitent pas à, les piétons, les véhicules, les panneaux de signalisation et les marqueurs routiers, répartis en 8 catégories principales.
La capacité à segmenter correctement ces images en temps réel est fondamentale pour la prise de décision autonome du véhicule.

Ce document technique explore les méthodes et technologies actuelles de segmentation d'images, offre une analyse détaillée du modèle UNet que nous avons choisi pour un premier prototype, et discute des résultats obtenus ainsi que des améliorations potentielles.
En fournissant une vue d'ensemble de l'état de l'art et en détaillant notre approche spécifique, cette note vise à partager les avancées de notre équipe avec le reste de l'entreprise et à documenter nos processus pour les phases de développement futures.

# 2. Synthèse de l’état de l'art de la segmentation d'image binaire et multi-class

La segmentation d'image est une composante critique de la vision par ordinateur, utilisée pour diviser une image en plusieurs segments ou régions, chacune contenant des pixels avec certaines caractéristiques communes. Cette technique est particulièrement pertinente dans des domaines exigeant une compréhension fine de l'image, comme la navigation des véhicules autonomes où il faut distinguer routes, véhicules, piétons et autres éléments statiques ou dynamiques. Nous examinerons ici une gamme d'approches et d'architectures de segmentation, tout en discutant des aspects pratiques comme le fine tuning et l'annotation des données avec des références spécifiques au dataset CityScapes.

## Approches de Segmentation d'Image

### Segmentation Binaire
1. **Seuillage (Thresholding)**: Divise les pixels de l'image en deux groupes (avant-plan et arrière-plan) en fonction d'un seuil de luminosité.
2. **Méthodes basées sur les contours**: Détectent les changements de couleur ou de texture pour identifier les frontières des objets.
3. **Techniques basées sur la région**: Fusionnent les régions voisines qui ont des propriétés similaires.

### Segmentation Multi-class
1. **Segmentation sémantique**: Chaque pixel est classé dans une des classes prédéfinies, sans distinction entre les instances de la même classe.
2. **Segmentation d'instance**: Identifie non seulement les classes, mais aussi les instances distinctes de chaque classe dans l'image.

## Architectures Principales en Deep Learning

1. **Fully Convolutional Networks (FCN)**: Remplace les couches entièrement connectées par des couches convolutionnelles pour traiter des images de taille variable.
2. **U-Net**: Structure en forme de U avec des connexions de saut entre les couches de downsampling et upsampling, idéale pour les images médicales et autres images de petite taille.
3. **SegNet**: Similaire à U-Net mais utilise des indices de pooling max pour la segmentation de scène efficace dans les images de résolution plus élevée.
4. **DeepLab**: Utilise des modules atrous pour capturer le contexte à différentes échelles et améliorer la segmentation des frontières.
5. **PSPNet (Pyramid Scene Parsing Network)**: Emploie un module de pooling pyramidale pour saisir des informations contextuelles à différentes échelles.

### Fonctionnement des Convolutions Atrous

La convolution atrous modifie une convolution standard en insérant des espaces, ou des "trous", dans le noyau de convolution. Ces trous sont répartis de manière égale entre les éléments actifs du noyau de convolution. Par exemple, une convolution dilatée avec un taux de dilatation de 2 insérera un espace de un pixel entre chaque élément du noyau, augmentant ainsi la taille effective du noyau sans augmenter le nombre de poids du noyau.

#### Paramètres de la Convolution Atrous

- **Taux de dilatation** : Il spécifie l'intervalle entre les éléments du noyau. Un taux de dilatation de 1 correspond à une convolution standard, tandis qu'un taux supérieur augmente la dispersion du noyau.
- **Taille du noyau** : Même avec un taux de dilatation élevé, la taille physique du noyau ne change pas, mais l'espace couvert par le noyau sur l'entrée augmente.

#### Avantages des Convolutions Atrous

1. **Augmentation du champ réceptif** : Permet au noyau de couvrir une plus grande zone de l'entrée, ce qui aide à intégrer des contextes plus larges sans augmentation de coût computationnel.
2. **Préservation de la résolution de l'image** : Contrairement aux approches utilisant des couches de pooling qui réduisent la dimensionnalité de l'image (et donc sa résolution), les convolutions atrous permettent de maintenir la résolution tout en captant des informations à différentes échelles.
3. **Efficacité dans l'utilisation des paramètres** : Elles permettent une analyse détaillée des images sans augmenter le nombre de paramètres, ce qui est bénéfique pour les modèles profonds où la capacité computationnelle peut être une contrainte.

## Fine Tuning dans la Segmentation d'Images

Le fine tuning est une technique où un modèle pré-entraîné sur un grand dataset (comme ImageNet) est ajusté pour des tâches spécifiques avec moins de données. Il y a deux approches principales :
- **Fine Tuning Complet**: Toutes les couches du réseau sont ajustées sur le nouveau dataset.
- **Fine Tuning Partiel**: Seules les couches supérieures du réseau sont ajustées, les premières couches capturant des caractéristiques générales étant gardées telles quelles.

## Annotation des Données et CityScapes

Annoter les données représente une tâche coûteuse et laborieuse, essentielle à la performance des modèles de segmentation. CityScapes constitue une base de données largement employée pour la segmentation d'images urbaines, comprenant des annotations d'images de scènes urbaines européennes. Bien qu'utile pour former des modèles de navigation autonome en milieu urbain, ce dataset présente une limite : il ne couvre que les zones urbaines en Allemagne, ce qui en fait une base relativement restreinte.

# 3. Présentation des différentes approches possibles en segmentation d'image multi-class

## U-Net
**Avantages**:
- **Rapidité**: U-Net est réputé pour sa rapidité d'exécution, ce qui le rend idéal pour un développement de POC rapide.
- **Efficacité avec peu de données**: Très efficace pour apprendre à partir de petites quantités de données grâce à son mécanisme de transfert d'apprentissage entre les couches correspondantes.

**Inconvénients**:
- **Précision limitée pour des classes multiples**: Bien qu'efficace pour les images où les régions à segmenter sont bien définies et moins complexes, sa performance peut être limitée dans des scénarios multi-class avec de nombreuses catégories ou des imbrications complexes.

## SegNet
**Avantages**:
- **Efficacité en mémoire**: Utilise moins de paramètres que d'autres architectures grâce à son mécanisme de pooling indices pour la récupération des localisations des maxima.
- **Bonne performance sur des scènes urbaines complexes**: A montré de bons résultats sur des datasets comme CamVid, qui sont proches en complexité à CityScapes.

**Inconvénients**:
- **Temps d'inférence plus long**: Bien que plus efficace en mémoire, les performances en temps réel peuvent être impactées, surtout sans optimisation hardware spécifique.

## DeepLab
**Avantages**:
- **Haute précision des frontières**: Utilise des convolutions atrous pour capturer des contextes à différentes échelles, améliorant ainsi la précision des frontières des objets.
- **Adaptabilité**: Capable de fonctionner avec différentes tailles d'entrée, ce qui le rend adaptable à diverses applications.

**Inconvénients**:
- **Complexité et coûts computationnels**: Nécessite plus de puissance de calcul, ce qui peut être un défi pour des applications embarquées comme dans les véhicules autonomes.

## PSPNet
**Avantages**:
- **Excellente capacité de généralisation**: Utilise une pyramide de pooling pour agréger les contextes à différentes échelles, ce qui aide à mieux généraliser sur des images non vues durant l'entraînement.
- **Robuste dans divers environnements**: Performe bien sur des scènes variées, y compris les scènes urbaines complexes.

**Inconvénients**:
- **Demandes en ressources élevées**: Comme DeepLab, PSPNet nécessite d'importantes ressources computationnelles pour l'entraînement et l'inférence, ce qui peut limiter son application dans des systèmes à faible consommation énergétique.

# 4. Présentation du modèle et de l'architecture retenue : U-Net avec augmentations

## Choix de l'Architecture U-Net

Pour le prototype (POC), le modèle U-Net a été retenu en raison de sa rapidité et de sa capacité à produire de bons résultats même avec des datasets de taille limitée. Ce modèle est particulièrement adapté aux tâches de segmentation où la précision des contours des objets est primordiale.

## Configuration du Modèle

Le modèle a été configuré et entraîné avec les paramètres suivants :
- **Epochs :** 100
- **Taux d'apprentissage :** 0.01 avec une dégradation exponentielle pour réduire ce taux au cours des epochs.
- **Taux de dropout :** 0.1, pour réduire le surajustement en ignorant aléatoirement certaines unités pendant l'apprentissage.
- **Taux de déclin :** 0.85, pour diminuer le taux d'apprentissage au fur et à mesure que l'entraînement progresse.
- **Momentum**: 0.9, utilisé avec l'optimiseur SGD pour améliorer la convergence.
- **Fonction de perte :** Categorical Crossentropy, adaptée pour les tâches de classification multi-classes.
- **Métriques :** Coefficient de Dice, Accurancy, et IoU moyen pour évaluer la performance du modèle.
- **Callbacks**: Incluent l'arrêt prématuré (patience de 10 epochs), la réduction du taux d'apprentissage sur plateau, et l'enregistrement du meilleur modèle basé sur la perte.

Tous les paramètres ont été définis suite à une recherche des meilleurs hyperparamètres utilisant le même dataset, mais appliquée à un modèle U-Net Mini. Ce modèle simplifié comprenait seulement deux couches d'encodage, deux couches de pont, trois couches de décodage, et une couche de sortie finale.

## Architecture du Modèle U-Net

Le modèle U-Net est conçu avec un chemin d'encodage (contraction) et un chemin de décodage (expansion), qui sont symétriques, permettant ainsi une segmentation précise au niveau pixel.

### Chemin d'Encodage
- **Couche 1**: Deux couches de convolution de 64 filtres chacune, avec des noyaux de taille (3x3), activation ReLU, initialisation He normal, et padding 'same'. Entre les convolutions, un dropout de 10% est appliqué pour réduire le surapprentissage.
- **Couche 2 à 4**: Chaque couche suit un schéma similaire à la première, doublant le nombre de filtres à chaque nouvelle couche (128, 256, 512) pour capter des caractéristiques plus complexes. Chaque couche de convolution est suivie d'une couche de pooling (2x2) pour réduire la dimensionnalité et augmenter le champ de réception.
- **Couche 5**: Similaire aux couches précédentes mais avec 1024 filtres, maximisant la capture des caractéristiques avant la transition vers le chemin de décodage.

### Chemin de Pont
- **Pont**: Utilise une dernière couche de pooling pour préparer la transition du chemin d'encodage au chemin de décodage.

### Chemin de Décodage
- **Couches 6 à 10**: Chaque couche commence par une convolution transposée pour augmenter la dimensionnalité, suivie par une concaténation avec la couche correspondante du chemin d'encodage (skip connection), permettant la récupération des caractéristiques spatiales perdues pendant la contraction. Le nombre de filtres dans chaque couche diminue progressivement (512, 256, 128, 64, 32).

### Couche de Sortie
- **Sortie**: Une couche de convolution de taille (1x1) avec une activation softmax pour classifier chaque pixel dans l'une des classes prévues.

## Importance des Augmentations des Données

### Augmentations Géométriques
Ces transformations modifient la forme et la position des objets dans les images, ce qui aide le modèle à apprendre à reconnaître les objets indépendamment de leur orientation ou de leur position dans l'image. Les méthodes utilisées incluent :
- Retournement horizontal
- Rotation et cisaillement
- Transformations élastiques
- Recadrage

### Augmentations Colorimétriques
Les augmentations colorimétriques modifient l'intensité et la distribution des couleurs, préparant le modèle à performer robustement sous différentes conditions d'éclairage et de visibilité :
- Ajustement de la luminosité
- Normalisation du contraste
- Modification de la saturation
- Ajout de bruit gaussien

## Raison d'Être des Augmentations

L'augmentation des données joue un rôle essentiel pour les modèles de vision par ordinateur, surtout dans le domaine de la conduite autonome où les variations des conditions environnementales et les problèmes matériels tels que les glitches ou les défauts de mise au point sont fréquents. Ces techniques renforcent la capacité de généralisation du modèle en reproduisant un éventail de scénarios, tout en minimisant les risques de surapprentissage. Bien que le dataset Cityscapes fournisse une vaste gamme de scènes urbaines pour l'entraînement, l'utilisation de méthodes d'augmentation des données enrichit encore davantage cette diversité, améliorant ainsi la robustesse du modèle face à des situations inédites.

## Intégration et Entraînement

Le modèle a été entraîné en utilisant un générateur de données, qui applique des augmentations de manière dynamique, offrant une grande variété d'exemples lors de chaque époque. Cette méthode est particulièrement efficace pour traiter des jeux de données volumineux comme Cityscapes tout en utilisant des ressources mémoire limitées.

Le modèle, tout en ayant une performance initiale avec un coefficient Dice de 0.85, montre des voies d'amélioration potentielles dans la précision et la capacité à généraliser à partir d'exemples plus divers.

# 6. Conclusion et Pistes d'Amélioration Envisageables

## Résumé des Performances Actuelles

Le modèle U-Net implémenté pour notre projet de véhicules autonomes a démontré une efficacité certaine avec un coefficient de Dice de 0.85. Bien que ce résultat soit prometteur pour un prototype, il révèle aussi des défis importants en termes de généralisation et de précision, essentiels pour la navigation sûre et efficace des véhicules autonomes dans des environnements urbains complexes. Les limites observées nécessitent une série d'améliorations stratégiques pour améliorer la performance globale du système.

## Pistes d'Amélioration Technique

### Révision et Extension de l'Architecture du Modèle

1. **Exploration de Nouvelles Architectures** : Le recours à des architectures plus récentes et plus performantes comme DeepLab v3+ ou PSPNet pourrait considérablement améliorer la précision grâce à leur capacité à mieux gérer les contextes variés et les détails fins des images.

2. **Augmentation de la Profondeur et de la Complexité du Modèle** : En augmentant la complexité de l'architecture U-Net, notamment par l'ajout de couches ou l'adaptation des tailles de filtre, nous pourrions capter des caractéristiques plus subtiles des images, ce qui est crucial pour la segmentation détaillée requise dans les scènes urbaines.

3. **Intégration de Modules d'Attention** : L'ajout de modules d'attention pourrait focaliser le processus d'apprentissage sur les zones les plus pertinentes des images, améliorant ainsi la précision de la segmentation en minimisant les interférences des éléments de fond moins importants.

#### Fonctionnement des Modules d'Attention

Un module d'attention fonctionne en attribuant dynamiquement des poids aux différentes zones d'une image, permettant ainsi au réseau de se concentrer davantage sur les éléments les plus informatifs pour la tâche à accomplir. Dans le contexte de la segmentation d'image, cela signifie que le modèle peut "apprendre" à ignorer les parties moins pertinentes de l'image et se concentrer sur les régions qui sont essentielles pour identifier correctement les différentes classes d'objets.

#### Intégration dans les Modèles de Segmentation

1. **Focalisation Sélective** : Les modules d'attention guident le modèle pour qu'il accorde plus d'importance aux caractéristiques importantes et moins aux bruits ou éléments de fond qui pourraient détourner l'apprentissage. Par exemple, dans une scène de rue, plutôt que de traiter uniformément tous les pixels, le modèle avec attention se concentrera peut-être davantage sur les zones contenant des piétons et des véhicules, qui sont critiques pour la décision de conduite autonome.

2. **Amélioration de la Précision** : En réduisant l'impact des interférences dues aux éléments non essentiels, les modules d'attention peuvent améliorer significativement la précision de la segmentation. Cela est particulièrement utile dans des environnements complexes où de multiples objets et textures se superposent ou se côtoient.

3. **Apprentissage plus Profond des Caractéristiques** : En se concentrant sur les caractéristiques pertinentes, les modules d'attention permettent également une exploration plus profonde des attributs subtils des objets, ce qui pourrait être masqué dans une approche d'apprentissage plus généralisée.

### Acquisition de Ressources de Données

1. **Achat de Datasets Plus Diversifiés** : Investir dans l'acquisition de datasets supplémentaires couvrant une plus grande variété de conditions géographiques, météorologiques, et temporelles enrichirait considérablement le training set. Cela pourrait inclure des scènes de différentes villes du monde, avec des variations saisonnières et des conditions de circulation diverses, offrant ainsi une base d'apprentissage plus robuste et généralisable.

### Amélioration de la Gestion des Données

1. **Enrichissement et Nettoyage du Dataset** : Un examen et une amélioration continus de la qualité des données d'entraînement sont essentiels. Cela comprend l'assurance de l'exactitude des annotations et l'augmentation des exemples de cas limites ou rares pour améliorer la robustesse du modèle.

## Vision à Long Terme

Les améliorations proposées visent non seulement à affiner la précision et la fiabilité du modèle de segmentation d'images mais aussi à garantir son intégration harmonieuse dans le système de conduite autonome global. Par ces initiatives, nous aspirons à développer un système capable de naviguer de manière autonome avec une sécurité et une efficacité maximales, répondant ainsi aux attentes croissantes en matière de véhicules intelligents dans des environnements urbains complexes. Ces efforts soutiendront également notre objectif à long terme de positionner Future Vision Transport comme un leader dans la technologie des véhicules autonomes.
