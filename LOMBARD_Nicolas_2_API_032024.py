from fastapi import FastAPI, HTTPException
from fastapi.middleware.cors import CORSMiddleware
from pydantic import BaseModel
from typing import Dict, Tuple
from PIL import Image
import numpy as np
import requests
from io import BytesIO
import base64
import time
from tensorflow.keras.models import load_model
from tensorflow.keras.preprocessing.image import img_to_array
from keras import backend as K
from keras.saving import register_keras_serializable
import os

# Création de l'application FastAPI
app = FastAPI()

# Configuration du middleware CORS pour autoriser les requêtes cross-origin
app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],  # Autorise toutes les origines
    allow_credentials=True,
    allow_methods=["POST"],  # Autorise uniquement les méthodes POST pour ce service
    allow_headers=["*"],  # Autorise tous les headers
)

@register_keras_serializable()
def dice_coeff(y_true, y_pred):
    """
    Calcul du coefficient Dice, une mesure de la similitude entre deux ensembles.

    Args:
        y_true (np.array): Les valeurs réelles.
        y_pred (np.array): Les valeurs prédites.

    Returns:
        float: Le coefficient Dice calculé.
    """
    smooth = 1.
    y_true_f = K.flatten(y_true)
    y_pred_f = K.flatten(y_pred)
    intersection = K.sum(y_true_f * y_pred_f)
    return (2. * intersection + smooth) / (K.sum(y_true_f) + K.sum(y_pred_f) + smooth)

# Chargement du modèle U-Net pré-entraîné
unet_model = load_model('model_prod.keras')
input_size = (512, 256)  # Définition de la taille d'entrée pour le modèle

def encode_image_to_base64(image: Image) -> str:
    """Encode une image PIL en chaîne base64.

    Args:
        image (Image): L'image à encoder.

    Returns:
        str: L'image encodée en base64.
    """
    buffered = BytesIO()
    image.save(buffered, format="PNG")
    return base64.b64encode(buffered.getvalue()).decode("utf-8")

def download_image(url: str) -> Image:
    """Télécharge une image depuis une URL.

    Args:
        url (str): L'URL de l'image à télécharger.

    Raises:
        HTTPException: En cas d'échec du téléchargement de l'image.

    Returns:
        Image: L'image téléchargée.
    """
    try:
        response = requests.get(url)
        response.raise_for_status()  # Vérifie les erreurs HTTP
        return Image.open(BytesIO(response.content))
    except requests.RequestException as e:
        raise HTTPException(status_code=400, detail=f"Erreur de téléchargement de l'image: {e}")

def change_image_colors(image: Image, class_colors: Dict[int, Tuple[int, int, int]]) -> Image:
    """Change les couleurs des classes spécifiées dans l'image.

    Args:
        image (Image): L'image dont les couleurs doivent être modifiées.
        class_colors (Dict[int, Tuple[int, int, int]]): Un dictionnaire associant les valeurs des classes aux couleurs.

    Returns:
        Image: L'image avec les couleurs des classes modifiées.
    """
    image_array = np.array(image)
    colored_image = np.zeros((image_array.shape[0], image_array.shape[1], 3), dtype=np.uint8)

    for class_value, color in class_colors.items():
        mask = image_array == class_value
        colored_image[mask] = color

    return Image.fromarray(colored_image)

def merge_images(original_image: Image, segmentation_image: Image, opacity: float) -> Image:
    """Fusionne deux images avec une certaine opacité.

    Args:
        original_image (Image): L'image originale.
        segmentation_image (Image): L'image de segmentation à superposer.
        opacity (float): L'opacité de l'image de segmentation.

    Returns:
        Image: L'image résultante de la fusion.
    """
    original_image_rgba = original_image.convert("RGBA")
    segmentation_image_rgba = segmentation_image.convert("RGBA")
    return Image.blend(original_image_rgba, segmentation_image_rgba, opacity).convert("RGB")

class Data(BaseModel):
    image_name: str
    class_colors: Dict[int, Tuple[int, int, int]]

@app.post("/predict/")
async def predict(data: Data):
    """Endpoint de prédiction qui traite une image et renvoie l'image originale et la prédiction de segmentation.

    Args:
        data (Data): Les données d'entrée comprenant le nom de l'image et les couleurs des classes.

    Raises:
        HTTPException: En cas d'erreur lors du traitement.

    Returns:
        dict: Un dictionnaire contenant l'image originale, l'image de prédiction et le temps de prédiction.
    """
    start_time = time.time()
    base_url = os.getenv('FLASK_URL', 'http://127.0.0.1:8081')
    image_url = f"{base_url}/image/{data.image_name}_leftImg8bit.png"
    segmentation_url = f"{base_url}/segmentation/{data.image_name}_gtFine_labelIds.png"

    try:
        original_image = download_image(image_url)
        segmentation_image = download_image(segmentation_url)

        image_for_pred = original_image.resize(input_size)
        image_for_pred_arr = img_to_array(image_for_pred) / 255.0
        image_for_pred_arr = np.expand_dims(image_for_pred_arr, axis=0)

        pred = unet_model.predict(image_for_pred_arr)
        pred_class = np.argmax(pred, axis=-1)[0]
        pred_resized = np.array(Image.fromarray(pred_class.astype('uint8')).resize(original_image.size, Image.NEAREST))

        colored_segmentation_image = change_image_colors(segmentation_image, data.class_colors)
        colored_pred_image = change_image_colors(Image.fromarray(pred_resized), data.class_colors)

        merged_image = merge_images(original_image, colored_segmentation_image, 0.5)

        return {
            "original_image": encode_image_to_base64(merged_image),
            "predict_image": encode_image_to_base64(colored_pred_image),
            "prediction_time": time.time() - start_time
        }

    except HTTPException as e:
        raise e
    except Exception as e:
        raise HTTPException(status_code=500, detail=str(e))

@app.get("/healthcheck")
def health_check():
    """Un simple endpoint pour vérifier la santé du service.

    Returns:
        dict: Un dictionnaire contenant le statut du service.
    """
    return {"status": "ok"}
