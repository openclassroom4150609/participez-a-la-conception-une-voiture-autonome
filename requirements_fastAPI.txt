fastapi
requests
Pillow
numpy==1.23.1
tensorflow==2.13.1
uvicorn