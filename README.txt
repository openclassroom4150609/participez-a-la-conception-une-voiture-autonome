pip install --upgrade typing_extensions



docker build -t registry.gitlab.com/openclassroom4150609/participez-a-la-conception-une-voiture-autonome/api:0.02 -f Dockerfile_fastapi .
docker push registry.gitlab.com/openclassroom4150609/participez-a-la-conception-une-voiture-autonome/api:0.02


docker build -t registry.gitlab.com/openclassroom4150609/participez-a-la-conception-une-voiture-autonome/site:0.02 -f Dockerfile_flask .
docker push registry.gitlab.com/openclassroom4150609/participez-a-la-conception-une-voiture-autonome/site:0.02
