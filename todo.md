### 1. **Conception du modèle de segmentation des images**

- **Tâches à réaliser** :
  - **Collecte des données** : Téléchargez le jeu de données à partir des liens fournis par Franck et ce concentrant sur les 8 catégories principales.
  - **Prétraitement des données** : Nettoyez et préparez les données pour l'entraînement, en tenant compte des contraintes de Franck concernant les catégories.
  - **Conception du modèle** : Utilisez Keras pour développer un modèle de segmentation d'images. Explorez les architectures de réseaux neuronaux convolutifs (CNN) telles que U-Net ou DeepLab pour cette tâche.
  - **Entraînement du modèle** : Entraînez votre modèle sur le jeu de données préparé. N'oubliez pas d'implémenter des techniques d'augmentation de données pour améliorer la généralisation du modèle.

- **Livrabes** :
  - Scripts de préparation des données et d'entraînement du modèle.
  - Modèle entraîné sauvegardé pour une utilisation future.

### 2. **Développement d'une API de prédiction**

- **Technologies** : Flask ou FastAPI pour la simplicité et la facilité d'intégration.
- **Fonctionnalités de l'API** :
  - Recevoir une image en entrée.
  - Utiliser le modèle entraîné pour prédire et renvoyer le masque de segmentation de l'image.

- **Déploiement sur le Cloud** : Azure, Heroku, ou PythonAnywhere pour faciliter l'accès et l'utilisation par Laura.

- **Livrabes** :
  - Code source de l'API.
  - Lien vers l'API déployée sur le Cloud.

### 3. **Création d'une application web Flask pour la présentation des résultats**

- **Fonctionnalités** :
  - Affichage des identifiants d'images disponibles.
  - Fonction de prédiction de segmentation à partir d'un ID d'image sélectionné, en utilisant l'API de prédiction.
  - Affichage de l'image réelle, du masque réel et du masque prédit.

- **Déploiement sur le Cloud** : Utilisez la même solution cloud que pour l'API pour une intégration et un accès simplifiés.

- **Livrabes** :
  - Code source de l'application Flask.
  - Lien vers l'application web déployée.

### 4. **Rédaction d'une note technique**

- **Contenu** :
  - Synthèse de l'état de l'art sur la segmentation d'images.
  - Détails sur l'architecture et le modèle choisis, justifiant vos choix.
  - Résultats obtenus, y compris l'impact des techniques d'augmentation de données.
  - Conclusion et pistes d'amélioration.

### 5. **Préparation d'une présentation de type PowerPoint**

- **Points à couvrir** :
  - Contexte et objectifs du projet.
  - Principes de segmentation et mesures de performance.
  - Présentation et comparaison des différents modèles testés.
  - Détails sur la mise en production, l'architecture de l'API et de l'application web.
  - Démonstration de l'application et de la prédiction de segmentation.

### Soutenance

- **Organisation** :
  - Présentation du projet et des résultats (20 minutes).
  - Discussion avec l'évaluateur sur les choix effectués (5 minutes).
  - Débriefing de la soutenance (5 minutes).

### Conseils pour le succès

- **Collaboration** : Restez en contact étroit avec Franck et Laura pour vous assurer que votre travail s'intègre bien avec le leur.
- **Documentation** : Documentez soigneusement votre code et vos processus pour faciliter la compréhension et la collaboration.
- **Tests et validations** : Testez soigneusement votre modèle et votre API pour vous assurer de leur fiabilité et de leur performance.


Regourper et non selection les labelId par group
test balence crossentropy loss
test total loss
VGG



Unet_Mini 10 Epoch
371/371 [==============================] - 445s 1s/step - loss: 0.5779 - dice_coeff: 0.4226 - mean_io_u: 0.4375 - accuracy: 0.4226 - val_loss: 0.5848 - val_dice_coeff: 0.4157 - val_mean_io_u: 0.4375 - val_accuracy: 0.4157 - lr: 0.0029