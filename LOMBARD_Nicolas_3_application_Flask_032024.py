from flask import Flask, request, render_template, send_from_directory
import os

app = Flask(__name__)

# Définir les chemins vers les répertoires contenant les images et les segmentations
val_images_dir = 'val_images'
val_seg_dir = 'val_segmentation'

# Précharger la liste des images prédéfinies disponibles pour l'affichage et le téléchargement
PREDEFINED_IMAGES = [
    img.replace('_leftImg8bit.png', '')
    for img in os.listdir(val_images_dir)
    if os.path.isfile(os.path.join(val_images_dir, img))
]

@app.route('/')
def index():
    """
    Afficher la page d'accueil avec les options pour visualiser les images prédéfinies
    et téléverser de nouvelles images.
    
    Retourne:
        render_template: Rendu de la page 'index.html' avec la liste des images prédéfinies.
    """
    return render_template('index.html', images=PREDEFINED_IMAGES,predict_url=os.environ.get('PREDICT_URL'))

@app.route('/image/<filename>')
def download_image(filename):
    """
    Permet le téléchargement d'une image spécifiée par son nom de fichier.
    
    Paramètres:
        filename (str): Le nom du fichier de l'image à télécharger.
        
    Retourne:
        send_from_directory: Renvoie le fichier image pour téléchargement si trouvé.
        str: Un message d'erreur si le fichier n'est pas trouvé.
    """
    filepath = os.path.join(val_images_dir, filename)
    if os.path.isfile(filepath):
        return send_from_directory(val_images_dir, filename, as_attachment=True)
    else:
        return "Fichier non trouvé", 404
    
@app.route('/segmentation/<filename>')
def download_segmentation(filename):
    """
    Permet le téléchargement d'un fichier de segmentation spécifié par son nom.
    
    Paramètres:
        filename (str): Le nom du fichier de segmentation à télécharger.
        
    Retourne:
        send_from_directory: Renvoie le fichier de segmentation pour téléchargement si trouvé.
        str: Un message d'erreur si le fichier n'est pas trouvé.
    """
    filepath = os.path.join(val_seg_dir, filename)
    if os.path.isfile(filepath):
        return send_from_directory(val_seg_dir, filename, as_attachment=True)
    else:
        return "Fichier non trouvé", 404
    
@app.get("/healthcheck")
def health_check():
    """Un simple endpoint pour vérifier la santé du service.

    Returns:
        dict: Un dictionnaire contenant le statut du service.
    """
    return {"status": "ok"}

if __name__ == '__main__':
    app.run(debug=True)
